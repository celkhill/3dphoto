from torch import optim
import SimpleITK as sitk
import re
import time
import numpy as np
import pdb
from torch_geometric.loader import DataLoader
from torch.nn import Linear, Sequential, Dropout
import torch.nn.functional as F
from torch_geometric.nn import ChebConv, GraphNorm
import torch
import pandas as pd
from torch_geometric.utils import to_dense_batch
from os import path, makedirs

DATA_DIR = path.split(path.realpath(__file__))[0]

class SegNet(torch.nn.Module):
    def __init__(self, in_channels = 9, out_channels = 27, Ks = [4,8], hidden_features = 200, use_weights = True, output_heat_map= False, dropout_value= 0.25,node_density_weights = False, use_texture = True, residuals = True):
        super().__init__()

        self.out_channels = out_channels
        self.use_weights = use_weights
        self.node_density_weights = node_density_weights
        self.dropout_value = dropout_value
        self.output_heat_map = output_heat_map
        self.use_texture = use_texture
        self.residuals = residuals

        
        if (not self.use_texture) & (in_channels == 9):
            raise ValueError("Set use_texture to False but also in_channels to 9.")


        #num of polynomials == K
        self.cheby1 = ChebConv(in_channels, hidden_features, K = Ks[0])#.to(self.devices[0])
        self.batchnorm1 = GraphNorm(hidden_features)#.to(self.devices[0])
        self.cheby2 = ChebConv(hidden_features, hidden_features, K = Ks[0])#.to(self.devices[0])
        self.batchnorm2 = GraphNorm(hidden_features)#.to(self.devices[0])
        self.cheby3 = ChebConv(hidden_features, hidden_features, K = Ks[0])#.to(self.devices[0])
        self.batchnorm3 = GraphNorm(hidden_features)#.to(self.devices[0])
        self.cheby4 = ChebConv(in_channels, hidden_features, K = Ks[1])#.to(self.devices[1])
        self.batchnorm4 = GraphNorm(hidden_features)#.to(self.devices[1])
        self.cheby5 = ChebConv(hidden_features, hidden_features, K = Ks[1])#.to(self.devices[1])
        self.batchnorm5 = GraphNorm(hidden_features)#.to(self.devices[1])
        self.cheby6 = ChebConv(hidden_features, hidden_features, K = Ks[1])#.to(self.devices[1])
        self.batchnorm6 = GraphNorm(hidden_features)#.to(self.devices[1])

        #feature selector MLP
        if self.use_weights:
            #feature selector for Low, medium, and high res features
            self.feature_selector = ChebConv(in_channels, 1, K = 1)

        #fc layers -- concatenated with lower level embeddings
        if self.residuals:
            self.fc0_lin = Linear(hidden_features *3 +in_channels,hidden_features)
        else:
            self.fc0_lin = Linear(hidden_features *3,hidden_features)
        self.fc0 = Sequential(self.fc0_lin, torch.nn.ReLU(), GraphNorm(hidden_features), Dropout(self.dropout_value))
        self.final_fc = Linear(hidden_features, self.out_channels)#.to(self.devices[0])

        #init our FC weights
        torch.nn.init.xavier_uniform_(self.fc0_lin.weight)
        torch.nn.init.xavier_uniform_(self.final_fc.weight)

    def forward(self, pos, features, batch, edge_indices):

        #grab our data from the loader
        if not self.use_texture:
            features = features[:,:3]

        x0 = torch.cat([pos,features], dim = 1)
        #one side
        out1 = self.batchnorm1(self.cheby1(x0, edge_indices, batch = batch).relu())
        out2 = self.batchnorm2(self.cheby2(out1, edge_indices, batch = batch).relu())
        out3 = self.batchnorm3(self.cheby3(out2, edge_indices, batch = batch).relu())

        # other side
        out4 = self.batchnorm4(self.cheby4(x0, edge_indices, batch = batch).relu())
        out5 = self.batchnorm5(self.cheby5(out4, edge_indices, batch = batch).relu())
        out6 = self.batchnorm6(self.cheby6(out5, edge_indices, batch = batch).relu())

        if not self.use_weights:
            hr = out1 + out4
            mr = out2 + out5
            lr = out3 + out6
        if self.use_weights:
            self.node_weights = self.feature_selector(x0, edge_indices, batch = batch).sigmoid()
            lw = self.node_weights[:,0,None]
            mw = self.node_weights[:,0,None]
            hw = self.node_weights[:,0,None]
            hr = out1 * hw + out4 * (1-hw)
            mr = out2 * mw + out5 * (1-mw)
            lr = out3 * lw + out6 * (1-lw)

        if self.residuals: 
            features = torch.cat([hr, mr, lr, x0], dim = 1)
        else:
            features = torch.cat([hr, mr, lr], dim = 1)
        out = self.fc0(features)
        out = self.final_fc(out)
        self.out = out

        return WeightedRegressionOutput(out, pos, batch, self.output_heat_map)


def WeightedRegressionOutput(out, pos, batch, output_heat_map = False):
    weights = GraphwiseSoftmax(out, batch)
    broadcasted_weighted_sum = []
    for x in range(weights.shape[1]):
        broadcasted_weighted_sum += [torch.mul(pos, weights[:,x].unsqueeze(1))]
    landmark_locs = torch.stack(broadcasted_weighted_sum, dim = 1)
    landmarks_per_batch = to_dense_batch(landmark_locs, batch)[0]
    final_landmarks = torch.sum(landmarks_per_batch, dim = 1)
    if output_heat_map:
        return final_landmarks, weights
    else:
        return final_landmarks

def GraphwiseSoftmax(data, batch_vector):
    '''
    Performs softmax over each batch so that sum == 1 per channel, per batch
    Assumes that data is a node-wise vector of all batches, concatenated
    batch vector is a integer vector indicating the batch of each node
    '''
    data_out = []
    #softmax along the node dimension -- picking one node per channel per batch
    for batch in torch.unique(batch_vector):
        locs = batch_vector == batch
        data_out += [F.softmax(data[locs,:], dim = 0)]
    data_out = torch.cat(data_out)
    return data_out

def FitToClosestPointOnMesh(pos, outputs):
    distance_between_landmarks_and_mesh = torch.min(torch.cdist(outputs.float(), pos.to(outputs.device).float()), dim = 1) # min distance to a point on the mesh from each landmark
    return distance_between_landmarks_and_mesh.indices

def encode_landmarks(data):
    data = data.to('cpu')
    #one hot encoding landmarks
    landmarks = torch.zeros(data.pos.shape[0], data.y.shape[1])
    for batch in np.unique(data.batch):
        locs = torch.where(data.batch == batch)[0]
        pos = data.pos[locs]
        landmark_locs = FitToClosestPointOnMesh(pos, data.y[batch,:,:])
        for row, col in zip(landmark_locs, torch.arange(len(landmark_locs))):
            landmarks[locs[row], col] = 1
    return landmarks

def ComputeErrorPerRow(test, truth):
    #euclidean distance per landmark, averaged over the batch
    distance = torch.mean(torch.diag(torch.cdist(truth.view(-1,3), test.view(-1,3))).view(-1, truth.shape[1]), dim = 0)
    return distance

def ComputeAverageDistanceAway(test, data):
    data = data.to('cpu')
    test = test.to('cpu')
    landmark_indices = []
    for batch in np.unique(data.batch):
        locs = torch.where(data.batch == batch)[0]
        landmark_indices += [torch.argmax(test[locs,:], dim = 0)]
    landmark_indices = torch.cat(landmark_indices)
    landmark_locs = data.pos[landmark_indices,:]
    distance = ComputeErrorPerRow(landmark_locs, data.y)
    return distance
def loss_func(criterion, global_landmarks, y, model):
    return torch.sqrt(criterion(global_landmarks, y.to(global_landmarks.device)))

def ScaleAllData(dataset_train, dataset_test, scaler = None):
    if scaler is None:
        pos = torch.cat([x.pos for x in dataset_train], dim = 0)
        x = torch.cat([x.x for x in dataset_train], dim = 0)
        scaler = StandardScaler()
        scaler.fit(torch.cat([pos, x], dim = 1))

    for data in dataset_train + dataset_test:
        features = scaler.transform(torch.cat([data.pos, data.x], dim = 1))
        data.pos = features[:,:3]
        data.x = features[:,3:]

    return dataset_train, dataset_test, scaler

class StandardScaler:

    def __init__(self, mean=None, std = None):
        #takes input of all training data to generate a mean and std for each of the features
        self.mean = mean
        self.std = std

    def fit(self, t):
        self.mean = torch.mean(t, dim = 0)
        self.std = torch.std(t, dim = 0)
    
    def transform(self, t):
        return (t - self.mean) / (self.std)

    def fit_transform(self, t):
        self.fit(t)
        return self.transform(t)

    def untransform(self, t):
        return (t * self.std) + (self.mean)

def RunInference(data, device = 'cpu', output_heat_map = False):
    model = SegNet(output_heat_map=output_heat_map).to(device)
    state_dict = torch.load(path.join(DATA_DIR,'./Checkpoint_490.dat'), map_location=device)['model_state_dict']
    model.load_state_dict(state_dict)
    model.eval()
    return model(data.to(device))