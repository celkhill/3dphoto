from tools.DataSetGraph import ReadPolyData, WritePolyData
import pdb
from tools.LandmarkingUtils import *
from os import path

if __name__ == "__main__":
    images = []
    for idx, filename in enumerate(images):
        print(f'On image {idx} out of {len(images)}...')
        image = ReadPolyData(filename)
        landmarks, cropped_image = RunInference(image, crop = True, return_cropped_image=True, fit_on_mesh = True)
        WritePolyData(landmarks,path.join(path.dirname(filename),'landmarks_new-{idx}.vtp'))
        WritePolyData(cropped_image,path.join(path.dirname(filename),'cropped_image-{idx}.vtp'))

